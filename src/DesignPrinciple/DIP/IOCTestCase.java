package DesignPrinciple.DIP;

/**
 * @time: 2022/10/20
 * @author: yuanyongan
 * @description:
 */
public abstract class IOCTestCase {
    public void run(){
        if (doTest()){
            System.out.println("Test Succeed");
        }else{
            System.out.println("Test Failed");
        }
    }
    public abstract boolean doTest();
}
