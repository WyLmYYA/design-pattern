package DesignPrinciple.DIP;

/**
 * @time: 2022/10/20
 * @author: yuanyongan
 * @description:
 */
public abstract class Phone {
    abstract void getParameter();
}
