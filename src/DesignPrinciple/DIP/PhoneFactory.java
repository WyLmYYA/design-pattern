package DesignPrinciple.DIP;

/**
 * @time: 2022/10/20
 * @author: yuanyongan
 * @description:
 */
public class PhoneFactory {
    private Phone phone;

    public PhoneFactory(Phone huawei){
        this.phone = huawei;
    }

    public void produce(){
        phone.getParameter();
        //...生产手机
    }
}
