package DesignPrinciple.DIP;

/**
 * @time: 2022/10/20
 * @author: yuanyongan
 * @description:
 */
public class Huawei extends Phone{
    public void getParameter(){
        // ... 业务需求
    }
}
