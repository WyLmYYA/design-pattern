package DesignPrinciple.DIP;

import java.util.ArrayList;
import java.util.List;

/**
 * @time: 2022/10/20
 * @author: yuanyongan
 * @description: 控制反转的例子
 */


public class IOC {
    private static final List<IOCTestCase> testCases = new ArrayList<>();

    public static void register(IOCTestCase testCase){
        testCases.add(testCase);
    }

    public void runTestCases(){
        for (IOCTestCase testCase: testCases){
            testCase.run();
        }
    }
}
