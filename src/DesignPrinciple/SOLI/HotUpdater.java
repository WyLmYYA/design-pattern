package DesignPrinciple.SOLI;

public interface HotUpdater {
    void hotUpdate();
}
