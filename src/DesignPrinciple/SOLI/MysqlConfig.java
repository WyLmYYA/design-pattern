package DesignPrinciple.SOLI;

import java.util.Map;

/**
 * @time: 2022/10/20
 * @author: yuanyongan
 * @description: 对mysql的配置文件进行操作
 */
public class MysqlConfig implements Viewer{
    private String configSource; // 配置中心（比如Zookeeper，Nacos等）用String简化

    private String address; // redis服务器地址

    private int timeout; // 连接超时时间

    private int maxTotal;

    public MysqlConfig(String configSource){
        this.configSource = configSource;
    }
    public String getAddress(){
        return this.address;
    }
    public void update(){
        // 从配置中心加载其他属性
    }

    @Override
    public String outputInPlainText() {
        return null;
    }

    @Override
    public Map<String, String> output() {
        return null;
    }
    // ...省略其他方法
}
