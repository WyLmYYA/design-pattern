package OOP.VirtualWalletAnemia.controller;

import OOP.VirtualWalletAnemia.service.VirtualWalletService;

import java.math.BigDecimal;

/**
 * @time: 2022/10/19
 * @author: yuanyongan
 * @description: 负责对service的服务进行调用，返回业务结果
 */
public class VirtualWalletController {
    // 通过构造函数或者IOC框架注入
    private VirtualWalletService virtualWalletService;

    // 查询余额
    public BigDecimal getBalance(Long walletId){
        return virtualWalletService.getBalance(walletId);
    }
    public void debit(Long walletId, BigDecimal amount){
        // 余额减少amount
        virtualWalletService.debit(walletId, amount);
    }
    public void credit(Long walletId, BigDecimal amount){
        // 余额增加amount
        virtualWalletService.credit(walletId, amount);
    }
    public void transfer(Long fromWalletId, Long toWalletId,BigDecimal amount){
        // 转账，一个支出账户，一个转入账户
        virtualWalletService.transfer(fromWalletId, toWalletId, amount);
    }
}
