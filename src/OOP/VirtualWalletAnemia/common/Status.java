package OOP.VirtualWalletAnemia.common;

public enum Status {
    TO_BE_EXECUTED,
    EXECUTING,
    FAILED,
    SUCCEED
}
