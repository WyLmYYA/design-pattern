package OOP.VirtualWalletAnemia.service;

import OOP.VirtualWalletAnemia.common.Status;
import OOP.VirtualWalletAnemia.entity.VirtualWalletEntity;
import OOP.VirtualWalletAnemia.entity.VirtualWalletTransactionEntity;
import OOP.VirtualWalletAnemia.repository.VirtualWalletRepository;
import OOP.VirtualWalletAnemia.repository.VirtualWalletTransactionRepository;

import java.math.BigDecimal;

/**
 * @time: 2022/10/19
 * @author: yuanyongan
 * @description: 实际业务处理流程，通过调用Repository对数据进行获取
 */
public class VirtualWalletService {
    // 通过构造函数或者IOC框架注入
    private VirtualWalletRepository walletRepo;
    private VirtualWalletTransactionRepository transactionRepo;
    public VirtualWalletEntity getWalletEntity(Long walletId){
        // 根据id返回钱包对象
        return walletRepo.getWalletEntity(walletId);
    }
    public BigDecimal getBalance(Long walletId){
        // 根据id返回钱包对象的余额
        return walletRepo.getBalance(walletId);
    }

    // 余额支出逻辑
    public void debit(Long walletId, BigDecimal amount){
        // 实际场景需要通过id获取entity
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        BigDecimal balance = walletEntity.getBalance();
        if (balance.compareTo(amount) > 0){
            walletRepo.updateBalance(walletId, balance.subtract(amount));
        }
    }

    // 余额收入逻辑
    public void credit(Long walletId, BigDecimal amount){
        VirtualWalletEntity walletEntity = walletRepo.getWalletEntity(walletId);
        BigDecimal balance = walletEntity.getBalance();
        walletRepo.updateBalance(walletId, balance.add(amount));
    }

    // 转账逻辑
    public void transfer(Long fromWalletId, Long toWalletId, BigDecimal amount){
        VirtualWalletTransactionEntity transactionEntity = new VirtualWalletTransactionEntity();
        transactionEntity.setAmount(amount);
        transactionEntity.setCreateTime(System.currentTimeMillis());
        transactionEntity.setFromWalletId(fromWalletId);
        transactionEntity.setToWalletId(toWalletId);
        transactionEntity.setStatus(Status.TO_BE_EXECUTED);
        Long transactionId = transactionRepo.saveTransaction(transactionEntity);
        try{
            debit(fromWalletId, amount);
            credit(toWalletId, amount);
        }catch (Exception e){
            transactionRepo.updateStatus(transactionId, Status.FAILED);
            // .. 处理异常
        }
        transactionRepo.updateStatus(transactionId, Status.SUCCEED);
    }


}
