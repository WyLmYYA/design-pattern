package OOP.VirtualWalletAnemia.entity;

import java.math.BigDecimal;

/**
 * @time: 2022/10/19
 * @author: yuanyongan
 * @description: 钱包实例，只对数据进行存储，不进行业务需求开发
 */
public class VirtualWalletEntity {
    private Long id;
    private Long creatTime;
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Long creatTime) {
        this.creatTime = creatTime;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
