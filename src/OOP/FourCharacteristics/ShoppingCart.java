package OOP.FourCharacteristics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @time: 2022/10/17
 * @author: yuanyongan
 * @description: 该类是阐述get和set中的一些特殊场景以及对应的解决方法
 */
class ShoppingCartItem{
    // 购物车每个商品的类，这里只是为了代码方便，代码省略...
}
public class ShoppingCart {
    private int itemCount; // 购物车中的商品数目
    private double totalPrice; // 总价钱
    private List<ShoppingCartItem> items = new ArrayList<>(); // 商品明细

    public int getItemCount(){return this.itemCount;}

    public void setItemCount(int itemCount){
        this.itemCount = itemCount;
    }
    public double getTotalPrice(){
        return this.totalPrice;
    }
    public void setTotalPrice(double totalPrice){
        this.totalPrice = totalPrice;
    }

    public List<ShoppingCartItem> getItems() {
        return Collections.unmodifiableList(this.items);
//        return items;
    }

    public void setItems(List<ShoppingCartItem> items) {
        this.items = items;
    }
}
