package OOP.FourCharacteristics;

import java.math.BigDecimal;

/**
 * @time: 2022/10/17
 * @author: yuanyongan
 * @description: 面向对象的Encapsulation，这里的对象是一个虚拟钱包的例子
 */
public class Wallet {
    private String id;
    private  long createTime;
    private BigDecimal balance; // 银行卡余额一般是浮点类型，为了保证精度，需要用BigDecimal类型
    private long balanceLastModifiedTime; // 余额最近发生变化的时间
    public Wallet(){
    }
    public String getId() {
        return id;
    }
    public long getCreateTime() {
        return createTime;
    }
    public BigDecimal getBalance() {
        return balance;
    }
    public long getBalanceLastModifiedTime() {
        return balanceLastModifiedTime;
    }
    public void increaseBalance(BigDecimal increaseAmount){
        if (increaseAmount.compareTo(BigDecimal.ZERO) > 0){
            this.balance.add(increaseAmount);
            this.balanceLastModifiedTime = System.currentTimeMillis();
        }

    }
    public void decreaseBalance(BigDecimal decreaseAmount){
        if (decreaseAmount.compareTo(BigDecimal.ZERO) > 0 && decreaseAmount.compareTo(this.balance) > 0){
            this.balance.subtract(decreaseAmount);
            this.balanceLastModifiedTime = System.currentTimeMillis();
        }
    }

}
