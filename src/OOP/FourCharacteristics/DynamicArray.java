package OOP.FourCharacteristics;

/**
 * @time: 2022/10/17
 * @author: yuanyongan
 * @description: 面向对象的多态性Polymorphism和继承性Inheritance，这里的对象是一个动态数组的例子
 */
public class DynamicArray {
    private static final int DEFAULT_CAPACITY = 10;
    protected int size = 0;
    protected int capacity = DEFAULT_CAPACITY;
    protected Integer[] elements = new Integer[DEFAULT_CAPACITY];

    public int size() {
        return this.size;
    }

    public void add(Integer e){
        ensureCapacity();
        elements[size++] = e;
    }
    protected void ensureCapacity() {
        // ...如果数组满了就扩容，代码省略...
    }
}

class SortedDynamicArray extends DynamicArray{
    @Override
    public void add(Integer e){
        ensureCapacity();
        for(int i = size - 1; i >= 0; --i){
            if (elements[i] > e){
                elements[i + 1] = elements[i];
            }else {
                elements[i + 1] = e;
                ++size;
                break;
            }
        }

    }
}
