package OOP.FourCharacteristics;

/**
 * @time: 2022/10/17
 * @author: yuanyongan
 * @description: 面向对象的抽象Abstraction，这里的对象是一个动物的例子
 */

interface Animal{
    void eat(String food);
    int getAge();
}
public class Dog implements Animal{
    @Override
    public void eat(String food) {

    }
    @Override
    public int getAge() {
        return 0;
    }
}
class Cat implements Animal{
    @Override
    public void eat(String food) {

    }
    @Override
    public int getAge() {
        return 0;
    }
}
