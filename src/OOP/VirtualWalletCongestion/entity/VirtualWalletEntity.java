package OOP.VirtualWalletCongestion.entity;

import java.math.BigDecimal;

/**
 * @time: 2022/10/19
 * @author: yuanyongan
 * @description: 钱包实例，只对数据进行存储，不进行业务需求开发
 */
public class VirtualWalletEntity {
    private Long id;
    private Long creatTime;
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Long creatTime) {
        this.creatTime = creatTime;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void debit(BigDecimal amount){
        if (this.balance.compareTo(amount) > 0){
            this.balance.subtract(amount);
        }
    }
    public void credit(BigDecimal amount){
        if (amount.compareTo(BigDecimal.ZERO) > 0){
            this.balance.add(amount);
        }
    }
}
