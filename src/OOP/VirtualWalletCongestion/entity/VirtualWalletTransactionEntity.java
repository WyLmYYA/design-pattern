package OOP.VirtualWalletCongestion.entity;

import OOP.VirtualWalletCongestion.common.Status;

import java.math.BigDecimal;

/**
 * @time: 2022/10/19
 * @author: yuanyongan
 * @description: 交易记录类，也只记录数据，不进行业务需求开发
 */
public class VirtualWalletTransactionEntity {
    private Long transactionId;
    private Long createTime;
    private BigDecimal amount;//交易金额
    private Status status;// 交易类型
    private Long toWalletId; // 转入钱包账号
    private Long fromWalletId; // 转出钱包账号

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getToWalletId() {
        return toWalletId;
    }

    public void setToWalletId(Long toWalletId) {
        this.toWalletId = toWalletId;
    }

    public Long getFromWalletId() {
        return fromWalletId;
    }

    public void setFromWalletId(Long fromWalletId) {
        this.fromWalletId = fromWalletId;
    }
}
