package OOP.VirtualWalletCongestion.repository;

import OOP.VirtualWalletCongestion.entity.VirtualWalletEntity;

import java.math.BigDecimal;

/**
 * @time: 2022/10/19
 * @author: yuanyongan
 * @description: 对虚拟钱包对象的业务与数据库交互，获取数据
 */
public class VirtualWalletRepository {
    public VirtualWalletEntity getWalletEntity(Long walletId){
        // 通过id和SQL语句访问数据库得到数据
        return new VirtualWalletEntity();
    }

    public BigDecimal getBalance(Long walletId){
        return getWalletEntity(walletId).getBalance();
    }

    public void updateBalance(Long walletId, BigDecimal amount){
        // 这里应该通过SQL映射对钱包余额进行更新
        VirtualWalletEntity virtualWalletEntity = new VirtualWalletEntity();
        virtualWalletEntity.setBalance(amount);

    }

}
