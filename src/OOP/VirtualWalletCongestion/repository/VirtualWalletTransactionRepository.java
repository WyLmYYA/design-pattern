package OOP.VirtualWalletCongestion.repository;

import OOP.VirtualWalletCongestion.common.Status;
import OOP.VirtualWalletCongestion.entity.VirtualWalletTransactionEntity;

/**
 * @time: 2022/10/19
 * @author: yuanyongan
 * @description: 对交易记录对象的业务与数据库交互，获取数据
 */
public class VirtualWalletTransactionRepository {


    public Long saveTransaction(VirtualWalletTransactionEntity virtualWalletTransactionEntity){
        // SQL语句存储对象

        // 并返回全局唯一的id
        return 0L;
    }

    public void updateStatus(Long transactionId, Status status){
        // 对交易记录状态进行修改

    }
}
