package OOP.VirtualWalletCongestion.common;

public enum Status {
    TO_BE_EXECUTED,
    EXECUTING,
    FAILED,
    SUCCEED
}
